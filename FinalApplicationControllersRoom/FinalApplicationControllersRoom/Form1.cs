﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace FinalApplicationControllersRoom
{

    public partial class Form1 : Form
    {
        static DB_Editor editor = new DB_Editor();

        public Form1()
        {
            InitializeComponent();
            GC.Collect();
        }

        private void создатьСобытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void переносСобытияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelDelete.Visible = false;
            panelDelete.Enabled = false;
            panelDelete.SendToBack();
            panelTransfer.Visible = true;
            panelTransfer.Enabled = true;
            panelTransfer.BringToFront();
            GC.Collect();
        }

        private void panelTransfer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBoxHousing_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.function1(comboBoxHousing, numericUpDown5, comboBoxAudience, textBox3, textBox5, dateTimePicker1, comboBox1);
            GC.Collect();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonTransferOk_Click(object sender, EventArgs e)
        {
            
            DialogTransferForm f = new DialogTransferForm();
            f.ShowDialog(this);
            editor.function2(label34,label2,comboBox1,comboBoxAudience,comboBoxHousing,dateTimePicker1,comboBox2,dateTimePicker2,dataGridView1);
            GC.Collect();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void удалениеСобытияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelTransfer.Visible = false;
            panelTransfer.SendToBack();
            panelDelete.Visible = true;
            panelDelete.Enabled = true;
            panelDelete.BringToFront();
            GC.Collect();
        }

        private void groupBox5_Enter_1(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void оперативноеВыделениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelDelete.Visible = false;
            panelTransfer.Visible = false;
            panelEveryDaySelect.Visible = true;
            GC.Collect();

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            editor.function3(checkBox3,checkBox2,numericUpDown1,comboBox13,comboBox9,comboBox4,comboBox14,comboBox10,dateTimePicker6,dateTimePicker5,label29,dataGridView2,comboBox8,numericUpDown2,comboBox11,comboBox12,numericUpDown3);
            GC.Collect();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            editor.function4(dateTimePicker6,dateTimePicker5,comboBox12,textBox7,textBox6,dataGridView2,comboBox8,label29);
            GC.Collect();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label30_Click(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            editor.function5(label31,comboBox5,comboBox6,comboBox7,dateTimePicker3);
            GC.Collect();
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.zdarova(comboBox7, numericUpDown4, comboBox6);
            editor.privet(textBox1,textBox2,comboBox7,comboBox6,dateTimePicker3,comboBox5);
            GC.Collect();
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.privet(textBox1, textBox2, comboBox7, comboBox6, dateTimePicker3, comboBox5);
            GC.Collect();
        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            editor.privet(textBox1, textBox2, comboBox7, comboBox6, dateTimePicker3, comboBox5);
            GC.Collect();
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.privet(textBox1, textBox2, comboBox7, comboBox6, dateTimePicker3, comboBox5);
            GC.Collect();
        }



        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxAudience_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.privet(textBox3,textBox5,comboBoxHousing,comboBoxAudience,dateTimePicker1,comboBox1);
            GC.Collect();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            editor.privet(textBox3, textBox5, comboBoxHousing, comboBoxAudience, dateTimePicker1, comboBox1);
            GC.Collect();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.privet(textBox3, textBox5, comboBoxHousing, comboBoxAudience, dateTimePicker1, comboBox1);
            GC.Collect();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            editor.function6(label2,dataGridView1,dateTimePicker2,comboBox2,comboBox1,checkBox1,comboBoxHousing,comboBoxAudience);
            GC.Collect();
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void comboBox14_SelectedIndexChanged(object sender, EventArgs e)
        {
            editor.zdarova(comboBox14, numericUpDown3, comboBox4);
            GC.Collect();
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            editor.zdarova(comboBox14, numericUpDown3, comboBox4);
            GC.Collect();
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            editor.zdarova(comboBox7, numericUpDown4, comboBox6);
            editor.privet(textBox1, textBox2, comboBox7, comboBox6, dateTimePicker3, comboBox5);
            GC.Collect();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            editor.zdarova(comboBoxHousing, numericUpDown5, comboBoxAudience);
            GC.Collect();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
        // список всех классов (без БоардType и Building)
        public class BoardSize
        {

            public int width;
            public int height;
            public BoardSize()
            {
                width = 0;
                height = 0;
            }
            public BoardSize(int width, int height)
            {
                this.width = width;
                this.height = height;
            }
            override public string ToString()
            {
                return "1. width : " + width + "\n2. height : " + height + "\n";
            }
        }
        public class Number
        {
            public string name;
            public int floor;
            public string building;
            public Number()
            {
                name = "";
                floor = 0;
                building = "";
            }
            public Number(string name, int floor, string building)
            {
                this.name = name;
                this.floor = floor;
                this.building = building;
            }

            override public string ToString()
            {
                return "1. name : " + name + "\n2. floor : " + floor + "\n3. buolding : " + building + "\n";
            }
        }
        public class Faculty
        {
            public string fullName;
            public string shortName;
            public string contacts;
            public Faculty()
            {
                fullName = "";
                shortName = "";
                contacts = "";
            }
            public Faculty(string shortName, string fullName, string contacts)
            {
                this.fullName = fullName;
                this.shortName = shortName;
                this.contacts = contacts;
            }
            override public string ToString()
            {
                return "1. fullName : " + fullName + "\n2. shortName : " + shortName + "\n3. contacts : " + contacts + "\n";
            }
        }
        public class Room
        {
            public int capacity;
            public Faculty faculty;
            public int sockets;
            public bool screen;
            public string boardType;
            public Number number;
            public BoardSize boardSize;
            public Room()
            {
                capacity = 0;
                faculty = new Faculty();
                sockets = 0;
                screen = false;
                boardType = "";
                number = new Number();
                boardSize = new BoardSize();
            }
            
            public Room(int capacity, Faculty faculty, int sockets, bool screen, string boardType, Number number, BoardSize boardSize)
            {
                this.capacity = capacity;
                this.faculty = faculty;
                this.sockets = sockets;
                this.screen = screen;
                this.boardType = boardType;
                this.number = number;
                this.boardSize = boardSize;
            }
            override public string ToString()
            {
                return "1. capacity : " + capacity + "\n2. faculty : \n" + ((faculty == null) ? "null" : faculty.ToString()) + "3. sockets : " + sockets + "\n4. screen : " + screen + "\n5. boardType : " + boardType + "\n6. number : \n" + ((number == null) ? "null" : number.ToString()) + "7. boardSize : \n" + ((boardSize == null) ? "null" : boardSize.ToString());
            }
        }
        public class Event
        {
            public string name;
            public string responsibleFace;
            public int regularity;
            public Event()
            {
                name = "";
                responsibleFace = "";
                regularity = 0;
            }
            override public string ToString()
            {
                return "1. name : " + name + "\n2. responsibleFace : " + responsibleFace + "\n3. regularity : " + regularity + "\n";
            }
        }
        public class Pair
        {
            public int id;
            public string timeFrom;
            public string timeTo;
            public Pair()
            {
                id = 0;
                timeFrom = "";
                timeTo = "";
            }
            public Pair(int id, string timeFrom, string timeTo)
            {
                this.id = id;
                this.timeFrom = timeFrom;
                this.timeTo = timeTo;
            }
            public Pair(int id)
            {
                this.id = id;
                DB_Editor db_editor = new DB_Editor();
                db_editor.connection.Open();
                db_editor.command.CommandText = "select timeFrom,timeTo from Pair where id = \'" + id + "\';";
                db_editor.reader = db_editor.command.ExecuteReader();
                db_editor.reader.Read();
                timeFrom = db_editor.normalize(db_editor.reader["timeFrom"].ToString());
                timeTo = db_editor.normalize(db_editor.reader["timeTo"].ToString());
                db_editor.connection.Close();
            }
            override public string ToString()
            {
                return "1. id : " + id + "\n2. timeFrom : " + timeFrom + "\n3. timeTo : " + timeTo + "\n";
            }
        }
        public class Date
        {
            public Pair pair;
            public string date;
            public Date()
            {
                pair = new Pair();
                date = "";
            }
            public Date(Pair pair, string date)
            {
                this.pair = pair;
                this.date = date;
            }
            override public string ToString()
            {
                return "1. pair : \n" + ((pair == null) ? "null" : pair.ToString()) + "2. date : " + date + "\n";
            }
        }
        public class Link
        {
            public Date date;
            public Room room;
            public Event _event;
            public Link()
            {
                date = new Date();
                room = new Room();
                _event = new Event();
            }
            override public string ToString()
            {
                return "1. date : \n" + ((date == null) ? "null" : date.ToString()) + "2. room : \n" + ((room == null) ? "null" : room.ToString()) + "3. event : \n" + ((_event == null) ? "null" : _event.ToString()) + "\n";
            }

        }
        // класс для работі с базой данніх
        public class DB_Editor
        {
            public SqlConnection connection;
            public SqlCommand command = new SqlCommand();
            public SqlDataReader reader;
            public SqlDataReader reader01;
            //подключение к локальной бд и создание таблиц если их еще нету
            public void comand(string str)
            {
                connection.Open();
                command.CommandText = str;
                command.ExecuteNonQuery();
                connection.Close();
            }
            public void select_Building()
            {
                connection.Open();
                command.CommandText = "select * from Building;";
                reader01 = command.ExecuteReader();
                System.InvalidOperationException ex = null;
                while (ex == null)
                {
                    reader01.Read();
                    try
                    {
                        Console.Write(Int32.Parse(reader01["id"].ToString()) + " ");
                        Console.Write(normalize(reader01["name"].ToString()) + "\n");
                    }
                    catch (System.InvalidOperationException exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
            }
            public void select_Faculty()
            {
                connection.Open();
                command.CommandText = "select * from Faculty;";
                reader01 = command.ExecuteReader();
                System.InvalidOperationException ex = null;
                while (ex == null)
                {
                    reader01.Read();
                    try
                    {
                        Console.Write(normalize(reader01["shortName"].ToString()) + " ");
                        Console.Write(normalize(reader01["fullName"].ToString()) + " ");
                        Console.Write(normalize(reader01["contacts"].ToString()) + "\n");
                    }
                    catch (System.InvalidOperationException exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
            }
            public void select_Event()
            {
                connection.Open();
                command.CommandText = "select * from Event;";
                reader01 = command.ExecuteReader();
                System.InvalidOperationException ex = null;
                while (ex == null)
                {
                    reader01.Read();
                    try
                    {
                        Console.Write(normalize(reader01["title"].ToString()) + " ");
                        Console.Write(normalize(reader01["responsibleFace"].ToString()) + " ");
                        Console.Write(Int32.Parse(reader01["regularity"].ToString()) + "\n");
                    }
                    catch (System.InvalidOperationException exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
            }
            public void select_Room()
            {
                connection.Open();
                command.CommandText = "select * from BoardType,BoardSize,Room,Number,Building,Faculty where BoardType.id = boardType and BoardSize.id = boardSize and Number.number = Room.number and Faculty.id = faculty and Building.id = building;";
                reader01 = command.ExecuteReader();
                System.InvalidOperationException ex = null;
                while (ex == null)
                {
                    reader01.Read();
                    try
                    {
                        Console.Write(Int32.Parse(reader01["capacity"].ToString()) + " ");
                        Console.Write(normalize(reader01["fullName"].ToString()) + " ");
                        Console.Write(normalize(reader01["shortName"].ToString()) + " ");
                        Console.Write(normalize(reader01["contacts"].ToString()) + " ");
                        Console.Write(Int32.Parse(reader01["sockets"].ToString()) + " ");
                        Console.Write(Boolean.Parse(reader01["screen"].ToString()) + " ");
                        Console.Write(normalize(reader01["type"].ToString()) + " ");
                        Console.Write(normalize(reader01["number"].ToString()) + " ");
                        Console.Write(Int32.Parse(reader01["floor"].ToString()) + " ");
                        Console.Write(normalize(reader01["name"].ToString()) + " ");
                        Console.Write(Int32.Parse(reader01["width"].ToString()) + " ");
                        Console.Write(Int32.Parse(reader01["height"].ToString()) + "\n");
                    }
                    catch (System.InvalidOperationException exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
            }
            public void insert()
            {
                connection.Open();
                command.CommandText = "insert into BoardSize(id,width,height) values (1, 100, 100), (2,200,200), (3,300,300); insert into BoardType(id, type) values (1, 'Гриф'), (2,'Маркер'), (3,'Стенка'); insert into Building(number, name) values (1, 'Главный'), (2,'Северный'), (3,'Эконом'); insert into Event(id, name, responsibleFace, regularity) values (1, 'лекция по матану', 'Дубовой В.К.', 0), (2,'собрание орг комитета','Лаба В.',1), (3,'фуршет','Лаба В.',2); insert into Faculty(id, shortName, contacts, fullName) values (1, 'мех-мат', 'контаткты деканата мех-мата', 'факультет математики и информатики'), (2,'био','контаткты деканата биологов','биологический факультет'), (3,'филолог','контаткты деканата филологов','филологический факультет'); insert into Pair(id, timeFrom, timeTo) values (1, '8:00', '9:35'), (2,'9:55','11:30'), (3,'11:40','13:15'), (4,'13:25','15:00'); insert into Date(id, date, pair) values (1, '01.09.2017', 1), (2,'01.09.2017',2), (3,'01.09.2017',3); insert into Number(name, floor, building) values ('6-52', 6, 1), ('6-49',6,1), ('6-46',6,1); insert into Room(id, capacity, faculty, sockets, screen, boardType, number, boardSize) values (1, 30, 1, 1, 0, 1, '6-46', 1), (2,60,1,3,0,1,'6-49',2), (3,90,1,7,1,1,'6-52',3); insert into Link(date, room,event) values (1,2,3),(2,3,1),(3,1,2);";
                command.ExecuteNonQuery();
                connection.Close();
            }


            private void create(string str)
            {
                if (str == null)
                    str = "Data Source=DESKTOP-UH347TT\\SQLEXPRESS;Initial Catalog=database;Integrated Security=True";
                connection = new SqlConnection(str);
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                command.CommandText = "if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[BoardType]') AND type in (N'U')) create table BoardType(id int primary key, type nchar(16)); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[BoardSize]') AND type in (N'U')) create table BoardSize(id int primary key, width int, height int); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Building]') AND type in (N'U')) create table Building(id int primary key, name nchar(16)); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Event]') AND type in (N'U')) create table Event(id int primary key, title nchar(32), responsibleFace nchar(32), regularity int); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Faculty]') AND type in (N'U')) create table Faculty(id int primary key, fullName nchar(128), shortName nchar(16), contacts nchar(128)); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Pair]') AND type in (N'U')) create table Pair(id int primary key, timeFrom time, timeTo time); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Date]') AND type in (N'U')) create table Date(id int primary key, date date, pair int foreign key references Pair(id)); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Number]') AND type in (N'U')) create table Number(number nchar(16) primary key, floor int, building int foreign key references Building(id)); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Room]') AND type in (N'U')) create table Room(id int primary key, capacity int, faculty int foreign key references Faculty(id), sockets int, screen bit, boardType int foreign key references BoardType(id), number nchar(16) foreign key references Number(number), boardSize int foreign key references BoardSize(id)); if not exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[Link]') AND type in (N'U')) create table Link(date int, room int, event int foreign key references Event(id),primary key(date, room));";
                command.ExecuteNonQuery();
                connection.Close();
            }
            DB_Editor(string str)
            {
                create(str);
            }

            public DB_Editor()
            {
                create(null);
            }

            // удаление всех таблиц
            public void Delete()
            {
                connection.Open();
                command.CommandText = "drop table Link,Event,Date,Pair,Room,Number,BoardSize,BoardType,Building,Number;";
                command.ExecuteNonQuery();
                connection.Close();
            }
            //убирает лишние пробелы в конце строки
            public string normalize(string str)
            {
                string res = "";
                int len = str.Length, i = len - 1;
                for (; str[i] == ' '; i--) ;
                for (; i > -1; i--)
                    res = str[i] + res;
                return res;
            }
            public string building(int id)
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Open();
                }
                command.CommandText = "select name from Building where id = \'" + id + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                string res = normalize(reader["name"].ToString());
                connection.Close();
                return res;
            }
            public int building(string name)
            {
                connection.Open();
                command.CommandText = "select id from Building where name = \'" + name + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                int res = Int32.Parse(reader["id"].ToString());
                connection.Close();
                return res;
            }
            public string faculty(int id)
            {
                connection.Open();
                command.CommandText = "select shortName from Faculty where id = \'" + id + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                string res = normalize(reader["shortName"].ToString());
                connection.Close();
                return res;
            }

            public string type(int id)
            {
                connection.Open();
                command.CommandText = "select type from BoardType where id = \'" + id + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                string res = normalize(reader["type"].ToString());
                connection.Close();
                return res;
            }
            public int type(string name)
            {
                connection.Open();
                command.CommandText = "select id from BoardType where type = \'" + name + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                int res = Int32.Parse(reader["id"].ToString());
                connection.Close();
                return res;
            }
            public Room getRoomById(int id)
            {
                connection.Open();
                command.CommandText = "select name,capacity,fullName,shortName,contacts,sockets,screen,type,Room.number,floor,width,height from Room,Faculty,BoardType,Number,BoardSize,Building where Room.id = "+id+" and Faculty.id = faculty and Number.number = Room.number and Building.id = building and BoardType.id = boardType and BoardSize.id = boardSize;";
                reader = command.ExecuteReader();
                reader.Read();
                connection.Close();
                try
                {
                    Room res =  new Room(Int32.Parse(reader["capacity"].ToString()),new Faculty(reader["shortName"].ToString(), reader["fullName"].ToString(), reader["contacts"].ToString()), Int32.Parse(reader["sockets"].ToString()), reader["screen"].ToString() == "1", reader["type"].ToString(),new Number(normalize(reader["number"].ToString()), Int32.Parse(reader["floor"].ToString()), reader["name"].ToString()),new BoardSize(Int32.Parse(reader["width"].ToString()), Int32.Parse(reader["height"].ToString())));
                    connection.Close();
                    return res;
                }
                catch
                {
                    return null;
                }
            }

            public Room readRoom()
            {
                Room room = new Room();
                room.capacity = Int32.Parse(reader["capacity"].ToString());
                room.faculty.fullName = normalize(reader["fullName"].ToString());
                room.faculty.shortName = normalize(reader["shortName"].ToString());
                room.faculty.contacts = normalize(reader["contacts"].ToString());
                room.sockets = Int32.Parse(reader["sockets"].ToString());
                string str = reader["screen"].ToString();
                if (str == "0")
                    room.screen = false;
                else
                    room.screen = true;
                room.boardType = normalize(reader["type"].ToString());
                room.number.name = normalize(reader["number"].ToString());
                room.number.floor = Int32.Parse(reader["floor"].ToString());
                room.number.building = normalize(reader["name"].ToString());
                room.boardSize.width = Int32.Parse(reader["width"].ToString());
                room.boardSize.height = Int32.Parse(reader["height"].ToString());
                return room;
            }
            public Room read01Room()
            {
                Room room = new Room();
                room.capacity = Int32.Parse(reader01["capacity"].ToString());
                room.faculty.fullName = normalize(reader01["fullName"].ToString());
                room.faculty.shortName = normalize(reader01["shortName"].ToString());
                room.faculty.contacts = normalize(reader01["contacts"].ToString());
                room.sockets = Int32.Parse(reader01["sockets"].ToString());
                string str = reader01["screen"].ToString();
                if (str == "0")
                    room.screen = false;
                else
                    room.screen = true;
                room.boardType = normalize(reader01["type"].ToString());
                room.number.name = normalize(reader01["number"].ToString());
                room.number.floor = Int32.Parse(reader01["floor"].ToString());
                room.number.building = normalize(reader01["name"].ToString());
                room.boardSize.width = Int32.Parse(reader01["width"].ToString());
                room.boardSize.height = Int32.Parse(reader01["height"].ToString());
                return room;
            }
            public Date readDate()
            {
                Date date = new Date();
                date.pair.timeTo = normalize(reader["timeTo"].ToString());
                date.pair.timeFrom = normalize(reader["timeFrom"].ToString());
                date.date = normalize(reader["day"].ToString());
                return date;
            }
            public Event readEvent()
            {
                Event _event = new Event();
                _event.name = normalize(reader["title"].ToString());
                _event.responsibleFace = normalize(reader["responsibleFace"].ToString());
                _event.regularity = Int32.Parse(reader["regularity"].ToString());
                return _event;
            }
            public int new_Event_id()
            {
                int res = 0;
                connection.Open();
                command.CommandText = "select id from Event order by id;";
                reader = command.ExecuteReader();
                System.InvalidOperationException exx = null;
                while (exx == null)
                {
                    try
                    {
                        reader.Read();
                        res = Int32.Parse(reader["id"].ToString());
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        exx = ex;
                    }
                }
                connection.Close();
                return res + 1;
            }
            public int new_Date_id()
            {
                int res = 0;
                connection.Open();
                command.CommandText = "select id from Date order by id;";
                reader = command.ExecuteReader();
                System.InvalidOperationException exx = null;
                while (exx == null)
                {
                    try
                    {
                        reader.Read();
                        res = Int32.Parse(reader["id"].ToString());
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        exx = ex;
                    }
                }
                connection.Close();
                return res + 1;
            }
            public int new_Room_id()
            {
                int res = 0;
                connection.Open();
                command.CommandText = "select id from Room order by id;";
                reader = command.ExecuteReader();
                System.InvalidOperationException exx = null;
                while (exx == null)
                {
                    try
                    {
                        reader.Read();
                        res = Int32.Parse(reader["id"].ToString());
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        exx = ex;
                    }
                }
                connection.Close();
                return res + 1;
            }
            public int new_Date(int pair, string date)
            {
                int new_id = new_Date_id();
                comand("insert into Date(id,day,pair) values (" + new_id + ",\'" + date + "\'," + pair + ");");
                return new_id;
            }
            public int new_Building_id()
            {
                int res = 1;
                connection.Open();
                command.CommandText = "select id from Building order by id;";
                reader = command.ExecuteReader();
                System.InvalidOperationException exx = null;
                while (exx == null)
                {
                    try
                    {
                        reader.Read();
                        res = Int32.Parse(reader["id"].ToString());
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        exx = ex;
                    }
                }
                connection.Close();
                return res + 1;
            }
            public int new_Faculty_id()
            {
                int res = 1;
                connection.Open();
                command.CommandText = "select id from Faculty order by id;";
                reader = command.ExecuteReader();
                System.InvalidOperationException exx = null;
                while (exx == null)
                {
                    try
                    {
                        reader.Read();
                        res = Int32.Parse(reader["id"].ToString());
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        exx = ex;
                    }
                }
                connection.Close();
                return res + 1;
            }
            public void change_builіng(string w, string i)
            {
                try
                {
                    connection.Open();
                    command.CommandText = "select * from Building where name = \'" + w + "\';";
                    reader = command.ExecuteReader();
                    reader.Read();
                    int id = Int32.Parse(reader["id"].ToString());
                    connection.Close();
                    connection.Open();
                    command.CommandText = "delete from Building where name = \'" + w + "\';";
                    command.ExecuteNonQuery();
                    command.CommandText = "insert into Building(id,name) values (" + id + ",\'" + i + "\');";
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    connection.Close();
                }
            }
            public bool roomIsFree(int room_id, int pair_id, String[] dates)
            {
                if (dates.Length == 0)
                    return false;
                foreach (String date in dates)
                    if (event_in(Date_id(pair_id, date), room_id) != 0)
                        return false;
                return true;
            }
            public object[] allFreeRoomsInRangeAndWithPr(string[] dates, int capacity, int pair, int width, int height,int building, int sockets, int type, bool screen, bool free,DataGridView dataGridView2) // уюрать DataGridView dataGridView2
                                                                                                                                                                                                                               // необходимо зделать так, чтоб не все аудитории сразу загружались в оперативку, но то пожже.
            {
                Exception __ = null;
               System.Collections.ArrayList AllRooms = new System.Collections.ArrayList();
                // выбираются все аудитории кроме
                command.CommandText = "select shortname,Room.number,name,capacity,type,height,width,sockets,screen from Room,Building,Number,BoardType,BoardSize,Faculty where Faculty.id = faculty and Number.number = Room.number and Building.id = Number.building and BoardType.id = boardType and BoardSize.id = boardSize and capacity >= "+capacity+" and BoardSize.width >= "+width+ " and BoardSize.height >= " + height + " and sockets >= "+ sockets;
                if (type != 0)
                    command.CommandText += " and Room.boardType = " + type;
                if(screen)
                    command.CommandText += " and screen = 1";
                command.CommandText += ";";
                connection.Open();
                reader = command.ExecuteReader();
                for (; __ == null;)
                
                    try
                    {       
                    reader.Read();
                    Room new_room = new Room(Int32.Parse(reader["capacity"].ToString()), new Faculty(), Int32.Parse(reader["sockets"].ToString()), reader["screen"].ToString() == "1", normalize(reader["type"].ToString()), new Number(normalize(reader["number"].ToString()), 0, normalize(reader["name"].ToString())), new BoardSize(Int32.Parse(reader["width"].ToString()), Int32.Parse(reader["height"].ToString())));
                        new_room.faculty.shortName = normalize(reader["shortName"].ToString());
                        //dataGridView2.Rows.Add(new_room.number.building, new_room.number.name, new_room.capacity, new_room.boardType, new_room.boardSize.height + "x" + new_room.boardSize.width, new_room.sockets);
                            AllRooms.Add(new_room);
                        //все аудитории теперь здесь
                }
                catch (Exception _)
                {
                        __ = _;
                        connection.Close();
                        if (!free)
                        return AllRooms.ToArray();
                     for (int i = AllRooms.Count - 1; i >= 0; i--)
                        if (!roomIsFree(Room_id(((Room)AllRooms[i]).number.name, this.building(((Room)AllRooms[i]).number.building)), pair, dates))
                            AllRooms.RemoveAt(i);
                }
                return AllRooms.ToArray();
            }
            public void change_faculty(string s, string ns, string nf, string nc)
            {
                try
                {
                    connection.Open();
                    command.CommandText = "select * from Faculty where shortName = \'" + s + "\';";
                    reader = command.ExecuteReader();
                    reader.Read();
                    int id = Int32.Parse(reader["id"].ToString());
                    connection.Close();
                    connection.Open();
                    command.CommandText = "delete from Faculty where shortName = \'" + s + "\';";
                    command.ExecuteNonQuery();
                    command.CommandText = "insert into Faculty(id,shortName,fullName,contacts) values (" + id + ",\'" + ns + "\',\'" + nf + "\',\'" + nc + "\');";
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    connection.Close();
                }
            }
            public int event_in(int Date_id, int Room_id)
            {
                connection.Open();
                command.CommandText = "select event from Link where room = "+Room_id+" and date = "+Date_id+";";
                reader = command.ExecuteReader();
                reader.Read();
                try
                {
                    int res =  Int32.Parse(reader["event"].ToString());
                    connection.Close();
                    return res;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    return 0;
               }
            }
            public void function1(ComboBox comboBoxHousing, NumericUpDown numericUpDown5, ComboBox comboBoxAudience,TextBox textBox3, TextBox textBox5, DateTimePicker dateTimePicker1, ComboBox comboBox1)
            {
                zdarova(comboBoxHousing, numericUpDown5, comboBoxAudience);
                privet(textBox3, textBox5, comboBoxHousing, comboBoxAudience, dateTimePicker1, comboBox1);
            }
            // запросы
            // получение иинформации об аудитории и событии в ней, если оно есть.
            public Link first(string roomName, int buildin, int pair, string dat)
            {
                string building = this.building(buildin);
                Date date = new Date(new Pair(pair), dat);
                Link res = new Link();
                connection.Open();
                command.CommandText = "select id from Pair where timeFrom = \'" + date.pair.timeFrom + "\' and timeTo = \'" + date.pair.timeTo + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                try
                {
                    res.date.pair.id = Int32.Parse(reader["id"].ToString());
                    res.date.pair.timeTo = date.pair.timeTo;
                    res.date.pair.timeFrom = date.pair.timeFrom;
                }
                catch (System.InvalidOperationException ex)
                {
                    connection.Close();
                    return null;
                }
                res.date = date;
                connection.Close();
                connection.Open();
                command.CommandText = "select * from Room,Faculty,BoardType,Number,Building,BoardSize where faculty = Faculty.id and boardType = BoardType.id and Room.number = Number.number and building = Building.id and boardSize = BoardSize.id and Number.number = \'" + roomName + "\' and Building.name = \'" + building + "\';";
                reader = command.ExecuteReader();
                reader.Read();
                try
                {
                    res.room = readRoom();
                }
                catch (System.InvalidOperationException ex)
                {
                    connection.Close();
                    return null;
                }
                try
                {
                    connection.Close();
                    connection.Open();
                    command.CommandText = "select * from Link,Date,Pair,Room,Faculty,BoardType,Number,Building,BoardSize,Event where Link.date = Date.id and room = Room.id and event = Event.id and pair = Pair.id and faculty = Faculty.id and boardType = BoardType.id and Room.number = Number.number and building = Building.id and boardSize = BoardSize.id and Number.number = \'" + roomName + "\' and Date.day = \'" + date.date + "\' and Pair.id = " + date.pair.id + ";";
                    reader = command.ExecuteReader();
                    reader.Read();
                    res.date = readDate();
                    res._event = readEvent();
                }
                catch (System.InvalidOperationException ex)
                {
                    res._event = null;
                }
                connection.Close();
                return res;
            }
            public string date_normalize(string s)
            {//25.07.2017 -> 2017-07-25
                return "" + s[6] + "" + s[7] + "" + s[8] + "" + s[9] + "-" + s[3] + "" + s[4] + "-" + s[0] + "" + s[1];
            }
            // дописать факультет
            // возращает список пустых аудиторий подходящих под критерии.

            public System.Collections.ArrayList second(string date, int capacity, int pair, int width, int height, string number, int building, int sockets, int type, bool screen, bool free)
            {
                System.Collections.ArrayList rooms = new System.Collections.ArrayList();
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                System.InvalidOperationException exx = null;
                Link link;
                Room currentRoom;
                connection.Open();
                command.CommandText = "select * from BoardSize,BoardType,Building,Faculty,Number,Room where faculty = Faculty.id and boardType = BoardType.id and boardSize = BoardSize.id and Room.number = Number.number and building = Building.id;";
                reader01 = command.ExecuteReader();
                int i = 0;
                while (exx == null)
                {
                    try
                    {
                        reader01.Read();
                        currentRoom = read01Room();
                        rooms.Add(currentRoom);
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        exx = ex;
                    }
                }
                connection.Close();
                foreach (Room rom in rooms)
                {
                    if (rom.number.name == number)
                    {
                        connection.Close();
                        link = first(rom.number.name, this.building(rom.number.building), pair, date);
                        if (free == false)
                            res.Add(rom);
                        else if (link._event == null)
                            res.Add(rom);
                        continue;
                    }
                    if (rom.capacity < capacity)
                        continue;
                    if (rom.boardSize.width < width)
                        continue;
                    if (rom.boardSize.height < height)
                        continue;
                    if (building != 0 && !rom.number.building.Equals(this.building(building)))
                        continue;
                    if (rom.sockets < sockets)
                        continue;
                    if (type != 0 && !rom.boardType.Equals(this.type(type)))
                        continue;
                    if (screen == true && rom.screen == false)
                        continue;
                    connection.Close();
                    link = first(rom.number.name, this.building(rom.number.building), pair, date);
                    if (free == false)
                        res.Add(rom);
                    else if (link._event == null)
                        res.Add(rom);
                }
                connection.Close();
                return res;
            }
            public String[] regularDates(int regularity, System.DateTime a, System.DateTime b)
            {
                System.Collections.ArrayList dates = new System.Collections.ArrayList();
                while (a.DayOfYear <= b.DayOfYear)
                {
                    dates.Add(a);
                    switch (regularity)
                    {
                        case 0:
                            a = b.AddDays(1);
                            break;
                        case 1:
                            a = a.AddDays(1);
                            break;
                        case 2:
                            a = a.AddDays(7);
                            break;
                        case 3:
                            a = a.AddDays(14);
                            break;
                        case 4:
                            a = a.AddMonths(1);
                            break;
                    }
                }
                object[] semires = dates.ToArray();

                String[] res = new String[semires.Length];
                for (int i = 0; i < semires.Length; i++)
                    res[i] = date_normalize(((System.DateTime)semires[i]).ToString());
                return res;
            }
            // сортирует аудитории по false - факультету, true - вместимоссть;
            public Room[] Sort(object[] obj, bool fc, int cf, string number)
            {
                Room[] rooms = new Room[obj.Length];
                int j;
                for (j = 0; j < obj.Length; j++)
                    rooms[j] = (Room)obj[j];
                Room[] res = new Room[rooms.Length];
                Room temp;
                for (j = 0; j < rooms.Length; j++)
                    res[j] = rooms[j];
                int i = 0;
                if (fc)
                {
                    foreach (Room rom in rooms)
                        res[i++] = rom;
                    int len = res.Length;
                    for (i = 0; i < len - 1; i++)
                        for (j = i + 1; j < len; j++)
                            if (res[i].capacity > res[j].capacity)
                            {
                                temp = res[i];
                                res[i] = res[j];
                                res[j] = temp;
                            }
                    return res;
                }
                if (cf != 0)
                {
                    string shortName = this.faculty(cf);
                    int k = j = 0;
                    for (; k < res.Length; k++)
                        if (res[k].faculty.shortName.Equals(shortName))
                        {
                            temp = res[k];
                            res[k] = res[j];
                            res[j++] = temp;
                        }
                }
                for (i = 0; i < res.Length && res[i].number.name != number; i++) ;
                if(i!= res.Length)
                {
                    temp = res[i];
                    for (j = i; j > 0; j--)
                        res[j] = res[j - 1];
                    res[0] = temp;
                }
                return res;
            }


            //добавление нового события
            public int new_Event(string title, string responsibleFace, int regularity)
            {
                int new_id = new_Event_id();
                comand("insert into Event(id,title,responsibleFace,regularity) values (" + new_id + ",\'" + title + "\',\'" + responsibleFace + "\'," + regularity + ");");
                return new_id;
            }
            //добавление новой аудитории
            public void new_room(int capacity, int faculty, int sockets, bool screen, int boardType, string number, int boardSize, int floor, int building)
            {
                int new_id = new_Room_id();
                try
                {
                    comand("insert into Number(number,floor,building) values (\'" + number + "\'," + floor + "," + building + ");");
                    comand("insert into Room(id,capacity,faculty,sockets,screen,boardType,number,boardSize) values (" + new_id + "," + capacity + "," + faculty + "," + sockets + "," + (screen ? "1" : "0") + "," + boardType + ",\'" + number + "\'," + boardSize + ");");
                }
                catch (Exception ex)
                {
                    //Console.Write("Не Вішло создать аудиторию...\n");
                    connection.Close();
                }
            }
            //добавление нового корпуса
            public void new_building(string name)
            {
                int new_id = new_Building_id();
                comand("insert into Building(id,name) values (" + new_id + ",\'" + name + "\');");
            }
            public void new_faculty(string shortName, string fullName, string contacts)
            {
                int new_id = new_Faculty_id();
                comand("insert into Faculty(id,shortName,fullName,contacts) values (" + new_id + ",\'" + shortName + "\',\'" + fullName + "\',\'" + contacts + "\');");
            }
            public System.Collections.ArrayList rooms_in_building(string biuild, int floor)
            {
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                int duild_id = building(biuild);
                connection.Open();
                command.CommandText = "select * from Room,Number,BoardType,BoardSize,Faculty,Building where Number.floor = " + floor + " and Number.building = " + duild_id + " and BoardType.id = boardType and BoardSize.id = boardSize and Number.number = Room.number and Number.building = Building.id and Faculty.id = faculty;";
                reader = command.ExecuteReader();
                Exception ex = null;
                while (ex == null)
                {
                    try
                    {
                        reader.Read();
                        res.Add(readRoom());
                    }
                    catch (Exception exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
                return res;
            }
            public System.Collections.ArrayList allBoardSizes()
            {
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                connection.Open();
                command.CommandText = "select * from BoardSize;";
                reader = command.ExecuteReader();
                Exception ex = null;
                while (ex == null)
                {
                    try
                    {
                        reader.Read();
                        res.Add(new BoardSize(Int32.Parse(reader["width"].ToString()), Int32.Parse(reader["height"].ToString())));
                    }
                    catch (Exception exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
                return res;
            }
            public System.Collections.ArrayList allBoardTypes()
            {
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                connection.Open();
                command.CommandText = "select * from BoardType;";
                reader = command.ExecuteReader();
                Exception ex = null;
                while (ex == null)
                {
                    try
                    {
                        reader.Read();
                        res.Add(normalize(reader["type"].ToString()));
                    }
                    catch (Exception exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
                return res;
            }
            public System.Collections.ArrayList allBuildings()
            {
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                connection.Open();
                command.CommandText = "select * from Building;";
                reader = command.ExecuteReader();
                Exception ex = null;
                while (ex == null)
                {
                    try
                    {
                        reader.Read();
                        res.Add(normalize(reader["name"].ToString()));
                    }
                    catch (Exception exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
                return res;
            }
            public System.Collections.ArrayList allFacultis()
            {
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                connection.Open();
                command.CommandText = "select * from Faculty;";
                reader = command.ExecuteReader();
                Exception ex = null;
                while (ex == null)
                {
                    try
                    {
                        reader.Read();
                        res.Add(new Faculty(normalize(reader["shortName"].ToString()), normalize(reader["fullName"].ToString()), normalize(reader["contacts"].ToString())));
                    }
                    catch (Exception exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
                return res;
            }
            public System.Collections.ArrayList allPairs()
            {
                System.Collections.ArrayList res = new System.Collections.ArrayList();
                connection.Open();
                command.CommandText = "select * from Pair;";
                reader = command.ExecuteReader();
                Exception ex = null;
                while (ex == null)
                {
                    try
                    {
                        reader.Read();
                        res.Add(new Pair(Int32.Parse(reader["id"].ToString()), normalize(reader["timeFrom"].ToString()), normalize(reader["timeTo"].ToString())));
                    }
                    catch (Exception exx)
                    {
                        ex = exx;
                    }
                }
                connection.Close();
                return res;
            }
            public int Date_id(int pair_id, string date)
            {
                try
                {
                    connection.Open();
                    command.CommandText = "select id from Date where day = \'" + date + "\' and pair = " + pair_id + ";";
                    reader = command.ExecuteReader();
                    reader.Read();
                    int id = Int32.Parse(reader["id"].ToString());
                    connection.Close();
                    return id;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    return 0;
                }
            }
            public int Room_id(string name, int building_id)
            {
                try
                {
                    connection.Open();
                    command.CommandText = "select Room.id from Room,Number,Building where Room.number = Number.number and Number.building = Building.id and Number.number = \'" + name + "\' and Building.id = " + building_id + ";";
                    reader = command.ExecuteReader();
                    reader.Read();
                    int id = Int32.Parse(reader["id"].ToString());
                    connection.Close();
                    return id;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    return 0;
                }
            }
            public int Event_id(string title, string respons)
            {
                try
                {
                    connection.Open();
                    command.CommandText = "select id from Event where title = \'" + title + "\' and responsibleFace = \'" + respons + "\';";
                    reader = command.ExecuteReader();
                    reader.Read();
                    int id = Int32.Parse(reader["id"].ToString());
                    connection.Close();
                    return id;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    return 0;
                }
            }
            public void new_link(int date, int room, int _event)
            {
                connection.Open();
                command.CommandText = "select * from Link where date = " + date + " and room = " + room + " and event = " + _event + ";";
                reader = command.ExecuteReader();
                reader.Read();
                try
                {
                    string fusk = reader["date"].ToString();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    connection.Open();
                    command.CommandText = "insert into Link(date,room,event) values (" + date + "," + room + "," + _event + ");";
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Open();
                }
                connection.Close();
            }
            public void delete_link(int date, int room)
            {
                connection.Open();
                command.CommandText = "delete from Link where date = " + date + " and room = " + room + ";";
                command.ExecuteNonQuery();
                connection.Close();
            }
            public void privet(TextBox textBox1, TextBox textBox2, ComboBox comboBox7, ComboBox comboBox6, DateTimePicker dateTimePicker3, ComboBox comboBox5)
            {// show ivent in soom room
                textBox1.Text = "";
                textBox2.Text = "";
                int building;
                try
                {
                    building = comboBox7.SelectedIndex + 1;
                }
                catch (Exception ex)
                {
                    building = 0;
                }
                string number;
                try
                {
                    number = comboBox6.SelectedItem.ToString();
                }
                catch (Exception ex)
                {
                    number = null;
                }
                string date = date_normalize(dateTimePicker3.Value.Date.ToString());
                int pair;
                try
                {
                    pair = Int32.Parse(comboBox5.SelectedItem.ToString()[0].ToString());
                }
                catch (Exception ex)
                {
                    pair = 0;
                }
                if (building != 0 && number != null && pair != 0)
                {
                    Link link = first(number, building, pair, date);
                    if (link._event != null)
                    {
                        textBox1.Text = link._event.name;
                        textBox2.Text = link._event.responsibleFace;
                    }
                    else
                    {
                        textBox1.Text = "нічого не заплановано";
                        textBox2.Text = "нема відповідальних";
                    }
                }
            }
            public void privet(TextBox textBox1, TextBox textBox2, ComboBox comboBox7, ComboBox comboBox6, DateTimePicker dateTimePicker3, DateTimePicker dateTimePicker4, ComboBox comboBox3, ComboBox comboBox5)
            {// show ivent in soom room
                textBox1.Text = "";
                textBox2.Text = "";
                int building;
                try
                {
                    building = comboBox7.SelectedIndex + 1;
                }
                catch (Exception ex)
                {
                    building = 0;
                }
                string number;
                try
                {
                    number = comboBox6.SelectedItem.ToString();
                }
                catch (Exception ex)
                {
                    number = null;
                }
                String[] dates = regularDates(comboBox3.SelectedIndex + 1,dateTimePicker3.Value, dateTimePicker4.Value);
                int pair;
                try
                {
                    pair = Int32.Parse(comboBox5.SelectedItem.ToString()[0].ToString());
                }
                catch (Exception ex)
                {
                    pair = 0;
                }
                if (building != 0 && number != null && pair != 0)
                {
                    for (int i = 0; i < dates.Length; i++)
                    {
                        Link link = first(number, building, pair, dates[i]);
                        if (link._event != null)
                        {
                            textBox1.Text+= link._event.name;
                            textBox2.Text+= link._event.responsibleFace; // zapataya
                        }
                    }
                    if(textBox1.Text == "")
                    {
                        textBox1.Text = "нічого не заплановано";
                        textBox2.Text = "нема відповідальних";
                    }
                }
            }
            public void zdarova(ComboBox building, NumericUpDown floor, ComboBox room)
            {
                if (building.SelectedIndex + 1 != 0 && building.SelectedIndex + 1 != 4)
                {
                    object[] arr = rooms_in_building(this.building(building.SelectedIndex + 1), (int)floor.Value).ToArray();
                    for (int i = 0; i < arr.Length; i++)
                        arr[i] = (object)((Room)arr[i]).number.name;
                    int index = room.SelectedIndex;
                    while (room.Items.Count > 0)
                        room.Items.RemoveAt(0);
                    room.Items.AddRange(arr);
                    if (room.Items.Count > index)
                        room.SelectedIndex = index;
                }
            }
            public void function2(Label label34,Label label2,ComboBox comboBox1, ComboBox comboBoxAudience,ComboBox comboBoxHousing, DateTimePicker dateTimePicker1, ComboBox comboBox2, DateTimePicker dateTimePicker2, DataGridView dataGridView1)
            {
                
                label34.Text = "";
                label2.Text = "";
                try
                {
                    int date_id = Date_id(Int32.Parse(comboBox1.SelectedItem.ToString()[0].ToString()), date_normalize(dateTimePicker1.Value.Date.ToString()));
                    int room_id = Room_id(comboBoxAudience.SelectedItem.ToString(), comboBoxHousing.SelectedIndex + 1);
                    Link link = first(comboBoxAudience.SelectedItem.ToString(), comboBoxHousing.SelectedIndex + 1, Int32.Parse(comboBox1.SelectedItem.ToString()[0].ToString()), date_normalize(dateTimePicker1.Value.Date.ToString()));
                    if (link._event == null)
                        return;
                    int date_id_new = Date_id(comboBox2.SelectedIndex + 1, date_normalize(dateTimePicker2.Value.Date.ToString()));
                    try
                    {
                        if (date_id_new == 0)
                            date_id_new = new_Date(comboBox2.SelectedIndex + 1, date_normalize(dateTimePicker2.Value.Date.ToString()));
                        try
                        {
                            int room_id_new = Room_id(dataGridView1.CurrentRow.Cells[1].Value.ToString(), building(dataGridView1.CurrentRow.Cells[0].Value.ToString()));
                            int event_id = Event_id(link._event.name, link._event.responsibleFace);
                            new_link(date_id_new, room_id_new, event_id);
                            label2.Text = "Готово (наверно)!";
                            label2.ForeColor = Color.DarkSeaGreen;
                        }
                        catch (Exception ex)
                        {
                            label2.Text = "Выберіть аудиторію!";
                            label2.ForeColor = Color.DarkRed;
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        label2.Text = "Выберіть номер пары!";
                        label2.ForeColor = Color.DarkRed;
                        return;
                    }
                    delete_link(date_id, room_id);
                    label2.Text = "Ready!";
                    label2.ForeColor = Color.SeaGreen;
                }
                catch (Exception ex)
                {
                    label2.Text = "Не всі поля заповнені!";
                    label2.ForeColor = Color.DarkRed;
                    return;
                }
            }
            public void function3(CheckBox checkBox3, CheckBox checkBox2, NumericUpDown numericUpDown1, ComboBox comboBox13, ComboBox comboBox9, ComboBox comboBox4, ComboBox comboBox14, ComboBox comboBox10, DateTimePicker dateTimePicker6, DateTimePicker dateTimePicker5, Label label29, DataGridView dataGridView2,ComboBox comboBox8, NumericUpDown numericUpDown2, ComboBox comboBox11, ComboBox comboBox12, NumericUpDown numericUpDown3)
            {
                label29.Text = "";
                while (dataGridView2.Rows.Count > 0)
                {
                    dataGridView2.Rows.RemoveAt(0);
                }
                int pair = comboBox8.SelectedIndex + 1;
                int capacity = (int)numericUpDown2.Value;
                int floor = (int)numericUpDown3.Value;
                int faculty = comboBox11.SelectedIndex + 1;
                if (faculty == 23)
                    faculty = 24;
                else if (faculty == 24)
                    faculty = 0;

                int regularity = comboBox12.SelectedIndex + 1;
                System.DateTime dateFrom = dateTimePicker5.Value;
                System.DateTime dateTo = dateTimePicker6.Value;
                String[] dates = regularDates(regularity, dateFrom, dateTo);

                int boardType = comboBox10.SelectedIndex + 1;
                if (boardType == 3) boardType = 0;
                int height, width;
                int building = comboBox14.SelectedIndex + 1;
                if (building == 4) building = 0;
                string number;
                try
                {
                    number = comboBox4.SelectedItem.ToString();
                }
                catch (Exception _)
                {
                    number = null;
                }
                if (number == "")
                    number = null;
                try
                {
                    height = Int32.Parse(comboBox9.SelectedItem.ToString());
                }
                catch (Exception _)
                {
                    height = 0;
                }
                try
                {
                    width = Int32.Parse(comboBox13.SelectedItem.ToString());
                }
                catch (Exception _)
                {
                    width = 0;
                }
                if (pair == 0)
                {
                    label29.Text = "Введіть номер пари!";
                    label29.ForeColor = Color.DarkRed;
                    return;
                }
                int sockets = (int)numericUpDown1.Value;
                bool free = checkBox2.Checked;
                bool screen = checkBox3.Checked;
                Room[] rooms = Sort(allFreeRoomsInRangeAndWithPr(dates, capacity, pair, width, height, building, sockets, boardType, screen, free, dataGridView2), false, faculty,number);
                for (int i = 0; i < rooms.Length; i++)
                    dataGridView2.Rows.Add(rooms[i].number.building, rooms[i].number.name, rooms[i].capacity, rooms[i].boardType, "" + rooms[i].boardSize.height + "x" + rooms[i].boardSize.width, rooms[i].sockets);
            }
            public void function4(DateTimePicker dateTimePicker6, DateTimePicker dateTimePicker5, ComboBox comboBox12, TextBox textBox7, TextBox textBox6, DataGridView dataGridView2, ComboBox comboBox8, Label label29)
            {
                label29.Text = "";
                int pair_id = comboBox8.SelectedIndex + 1;
                if (pair_id == 0)
                {
                    label29.Text = "Выберіть номер пары!";
                    label29.ForeColor = Color.DarkRed;
                    return;
                }
                int room_id;
                try
                {
                    room_id = Room_id(dataGridView2.CurrentRow.Cells[1].Value.ToString(), building(dataGridView2.CurrentRow.Cells[0].Value.ToString()));
                }
                catch (Exception _)
                {
                    label29.Text = "выбери=ете аулиторию!";
                    label29.ForeColor = Color.DarkRed;
                    return;
                }
                if (textBox6.Text == "" || textBox7.Text == "")
                {
                    label29.Text = "Необхідна інформація про подію!";
                    label29.ForeColor = Color.DarkRed;
                    return;
                }
                int regularity = comboBox12.SelectedIndex + 1;
                System.DateTime dateFrom = dateTimePicker5.Value;
                System.DateTime dateTo = dateTimePicker6.Value;
                String[] dates;
                try
                {
                    dates = regularDates(comboBox12.SelectedIndex, dateTimePicker5.Value, dateTimePicker6.Value);
                }
                catch (Exception _)
                {
                    label29.Text = "дата или регулярность введені не правильно!";
                    return;
                }
                if (!roomIsFree(room_id, pair_id, dates))
                {
                    label29.Text = "в якийс день ця аудиторія уже зайнята!"; // пусть еще віводит когда и кем
                    return;
                }
                int date_id;
                int event_id = Event_id(textBox6.Text, textBox7.Text);
                if (event_id == 0)
                    event_id = new_Event(textBox6.Text, textBox7.Text, 0);

                for (int i = 0; i < dates.Length; i++)
                {
                    date_id = Date_id(pair_id, dates[i]);
                    if (date_id == 0)
                        date_id = new_Date(pair_id, dates[i]);
                    new_link(date_id, room_id, event_id);
                }
                label29.Text = "Auditoria is videlina na vse dni!";
                label29.ForeColor = Color.DarkGreen;
            }
            public void function5(Label label31, ComboBox comboBox5, ComboBox comboBox6, ComboBox comboBox7, DateTimePicker dateTimePicker3)
            {
                label31.Text = "";
                try
                {
                    int date_id = Date_id(Int32.Parse(comboBox5.SelectedItem.ToString()[0].ToString()), date_normalize(dateTimePicker3.Value.Date.ToString()));
                    int room_id = Room_id(comboBox6.SelectedItem.ToString(), comboBox7.SelectedIndex + 1);
                    delete_link(date_id, room_id);
                    label31.Text = "Аудиторію звільнено!";
                    label31.ForeColor = Color.SeaGreen;
                }
                catch (Exception ex)
                {
                    label31.Text = "Не всі поля заповнені!";
                    label31.ForeColor = Color.DarkRed;
                    return;
                }
            }
            public void function6(Label label2, DataGridView dataGridView1, DateTimePicker dateTimePicker2, ComboBox comboBox2, ComboBox comboBox1 , CheckBox checkBox1, ComboBox comboBoxHousing, ComboBox comboBoxAudience)
            {
                label2.Text = "";

                while (dataGridView1.Rows.Count > 0)
                {
                    dataGridView1.Rows.RemoveAt(0);
                }

                string date = date_normalize(dateTimePicker2.Value.Date.ToString());
                int pair = comboBox2.SelectedIndex + 1;
                int building = comboBoxHousing.SelectedIndex + 1;
                string number = comboBoxAudience.Text;
                if (number == "")
                {
                    label2.Text = "Выберіть аудиторію!";
                    label2.ForeColor = Color.DarkRed;
                    return;
                }
                if (building == 0)
                {
                    label2.Text = "Введіть корпус!";
                    label2.ForeColor = Color.DarkRed;
                    return;
                }
                if (pair == 0)
                {
                    pair = (comboBox2.SelectedIndex = comboBox1.SelectedIndex) + 1;
                    if (pair == 0)
                    {
                        label2.Text = "Введіть номер пари!";
                        label2.ForeColor = Color.DarkRed;
                        return;
                    }
                }
                Link link = first(number, building, pair, date);
                bool free = checkBox1.Checked;
                System.Collections.ArrayList empty = second(date, 0, pair, 0, 0, number, 0, 0, 0, false, free);
                foreach (Room room in empty)
                    dataGridView1.Rows.Add(room.number.building, room.number.name, room.capacity, room.boardType, "" + room.boardSize.height + "x" + room.boardSize.width, room.sockets);
            }
        }
    }
}

   
